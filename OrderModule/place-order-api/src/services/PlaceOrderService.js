/* eslint-disable no-unused-vars */
const Service = require('./Service');

const ObjectID = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;

function getPlaceOrderDatabaseCollection(collectionName) {
  return new Promise(async function(resolve) {
    const connection = await MongoClient.connect('mongodb://db_place_order');
    const db = await connection.db('place-order');
    const collection = await db.collection(collectionName);
    resolve(collection);
  });
}

async function getPageOfDocuments(collection, sortBy, limit, pagingToken, filterQuery={}) {
  var upOrDown = -1;
  var op = '$lt';
  var defaultPagingToken = 'f'.repeat(24);
  if (sortBy === '+created') {
    upOrDown = 1;
    op = '$gt';
    defaultPagingToken = '0'.repeat(24);
  }
  if ( (typeof pagingToken) !== 'string' || pagingToken === '') {
    pagingToken = defaultPagingToken;
  }
  
  const pagingQuery = {_id: {[op]: ObjectID(pagingToken) }};
  const query = {...pagingQuery, ...filterQuery};
  const sortOrder = {_id: upOrDown};
  const cursor = await collection.find(query).sort(sortOrder).limit(limit);
  const documents = await cursor.toArray();
  let nextPagingToken = '';
  if (documents.length > 0) {
    nextPagingToken = documents[documents.length-1]['_id'];
  }
  return {
    pagingToken: nextPagingToken,
    [collection.collectionName]: documents
  };
}

/**
* Get a specific order
*
* id String id of order to return
* returns OrderResponse
* */
const getOrder = ({ id }) => new Promise(
  async (resolve, reject) => {
    try {
      const ordersCollection = await getPlaceOrderDatabaseCollection('orders');
      const order = await ordersCollection.findOne({'_id': ObjectID(id)});
      if (order === null) {
        throw {
          message: "No such order.",
          status: 404
        };
      }
      resolve(Service.successResponse(
        order,
      ));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

/**
* List all orders
*
* sortBy String Which field to sort by (optional)
* limit Integer Number of items to return (optional)
* pagingToken String Used to specify next page (optional)
* returns List
* */
const getOrders = ({ sortBy, limit, pagingToken }) => new Promise(
  async (resolve, reject) => {
    try {
      const collection = await getPlaceOrderDatabaseCollection('orders');
      const orders = await getPageOfDocuments(collection, sortBy, limit, pagingToken);
      resolve(Service.successResponse(orders));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

/**
* List the items that can be ordered
*
* sortBy String Which field to sort by (optional)
* limit Integer Number of items to return (optional)
* pagingToken String Used to specify next page (optional)
* returns inline_response_200
* */
const listItems = ({ sortBy, limit, pagingToken }) => new Promise(
  async (resolve, reject) => {
    try {
      const collection = await getPlaceOrderDatabaseCollection('items');
      const items = await getPageOfDocuments(collection, sortBy, limit, pagingToken);
      resolve(Service.successResponse(items));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

/**
* Place an order
*
* order Order  (optional)
* no response value expected for this operation
* */
const placeOrder = ({ orderRequest }) => new Promise(
  async (resolve, reject) => {
    try {
      const itemsCollection = await getPlaceOrderDatabaseCollection('items');
      const itemsCursor = await itemsCollection.find({}, {name: 1});
      const items = await itemsCursor.toArray();
      const itemNames = items.map(i => i['name']);
      const errors = [];
      orderRequest['itemsList'].forEach(item => {
        if (!itemNames.includes(item.name)){
          errors.push(
            `Item named '${item}' does not exist.`
          );
        }
      });

      const allowedFields = ['email', 'itemsList', 'restrictions', 'preferences'];
      Object.keys(orderRequest).forEach(field => {
        if (!allowedFields.includes(field))
          errors.push(
            `Field named '${field}' is not allowed in order.`
          );
      })

      if (errors.length > 0) {
        throw {
          message: {message: "Error in items or fields. Check spelling or case?",
            errors},
          status: 400
        };
      }

      const ordersCollection = await getPlaceOrderDatabaseCollection('orders');
      const result = await ordersCollection.insertOne(orderRequest);
      resolve(Service.successResponse(

        result["ops"][0]
      , 201));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

module.exports = {
  getOrder,
  getOrders,
  listItems,
  placeOrder,
};
