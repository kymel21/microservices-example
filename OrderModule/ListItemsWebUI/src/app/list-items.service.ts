import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListItemsService {

  constructor() { }

  // This will be replaced with a call to the back end...
  getItems(): Observable<String[]> {
    return of([
      "Peanut butter",
      "Tuna",
      "Cereal",
      "Pasta",
      "Pasta sauce",
      "Soup",
      "Canned fruit",
      "Canned vegetables"
      ])
  }
}